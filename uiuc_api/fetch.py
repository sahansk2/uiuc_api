"""
Utility methods to package UIUC web api data into lxml objects.
"""
import abc
from datetime import datetime
from itertools import takewhile

import requests
import re
import datetime as dt
from typing import Optional, Iterator
from concurrent.futures import ThreadPoolExecutor
from lxml import etree
from lxml.etree import Element
import warnings

API_URL = "http://courses.illinois.edu/cisapp/explorer/catalog"
QUARTERS = ("spring", "summer", "fall", "winter")


class ExplorerRequest:
    """
    DO NOT INSTANTIATE DIRECTLY. Use ExplorerRequestFactory.
    Represents a container of parameters for a request made to the Course Explorer API.
    """
    def __init__(self, year: Optional[str] = None, quarter: Optional[str] = None, subject: Optional[str] = None, number: Optional[str] = None, cascade: bool = False):
        self.year = year
        self.quarter = quarter
        self.subject = subject
        self.number = number
        self.cascade = cascade

    def to_request(self) -> str:
        request = API_URL + "/"
        request += "/".join(takewhile(lambda x: x is not None, (self.year, self.quarter, self.subject, self.number)))
        request += ".xml"

        if self.cascade:
            request += "?mode=cascade"
        return request

    def __repr__(self):
        return f"<ExplorerRequest(year={self.year}, quarter={self.quarter}, subject={self.subject}, number={self.number})>"

    def as_tuple(self):
        return self.year, self.quarter, self.subject, self.number, self.cascade


class ExplorerRequestFactory:
    """
    Factory to create ExplorerRequest objects from user-supplied parameters. This class does much of the input
    validation and parameter checking, and attempts to provide sensible defaults where necessary.
    For both users and developers of this project.
    """
    @staticmethod
    def fetch_request_quarter(
            year: Optional[str] = None,
            quarter: Optional[str] = None
    ) -> ExplorerRequest:
        """
        Construct a fetch request with course-granularity. A "helper object" that returns a valid set of
        parameters that you could feed into Course Explorer's API.

        :param year: (str or None). Specified year (YYYY). Defaults to current year.
        :param quarter: (str or None). Specified semester ("spring", "summer", "fall", "winter"). Defaults to "spring"
        :returns: Corresponding ExplorerRequest to use with fetch().
        """
        assert_all_str_or_none(year=year, quarter=quarter)
        year = validated_year(year)
        quarter = validated_quarter(quarter)
        return ExplorerRequest(year=year, quarter=quarter)

    @staticmethod
    def fetch_request_subject(
            year: Optional[str] = None,
            quarter: Optional[str] = None,
            subject: Optional[str] = None,
            cascade: bool = False
    ) -> ExplorerRequest:
        """
        Construct a fetch request with subject-granularity (subject/number combination)

        :param year: (str or None). Specified year (YYYY). Defaults to current year.
        :param quarter: (str or None). Specified semester ("spring", "summer", "fall", "winter"). Defaults to "spring"
        :param subject: (str). Specified subject code (ex. CS, BIOE, ...). Required.
        :param cascade: (bool). Specify whether to fetch in cascade mode. Defaults to "false".
        :returns: Corresponding ExplorerRequest to use with fetch().
        """
        assert_all_str_or_none(year=year, quarter=quarter)
        assert_all_str(subject=subject)
        year = validated_year(year)
        quarter = validated_quarter(quarter)
        return ExplorerRequest(year=year, quarter=quarter, subject=subject, cascade=cascade)

    @staticmethod
    def fetch_request_course(
            year: Optional[str] = None,
            quarter: Optional[str] = None,
            subject: Optional[str] = None,
            number: Optional[str] = None
    ) -> ExplorerRequest:
        """
        Construct a fetch request with course-granularity (subject/number combination)

        :param year: (str or None). Specified year (YYYY). Defaults to current year.
        :param quarter: (str or None). Specified semester ("spring", "summer", "fall", "winter"). Defaults to "spring"
        :param subject: (str). Specified course subject code (ex. CS, BIOE, ...). Required.
        :param number: (str). Specified course number. Required. Must be three digits.
                Example: for the course CS 225, "225" is the number.
        :returns: Corresponding request to use with fetch().
        """
        assert_all_str_or_none(year=year, quarter=quarter)
        assert_all_str(subject=subject, number=number)
        year = validated_year(year)
        quarter = validated_quarter(quarter)
        number = validated_number(number)
        return ExplorerRequest(subject=subject, number=number, quarter=quarter, year=year)


def validated_quarter(quarter: Optional[str]):
    quarter = quarter or "spring"
    if quarter.lower() not in QUARTERS:
        raise ValueError("Quarter must be one of {}. Got {}".format(", ".join(map(lambda x: repr(x), QUARTERS)), repr(quarter)))
    return quarter


def validated_year(year: Optional[str]):
    year = year or str(dt.datetime.now().year)
    datetime.strptime(year, "%Y")  # YYYY or bust
    return year


def validated_number(number: str):
    if not re.match(r"\d{3}", number):
        raise ValueError("Expected course number to be 3 digit string. Got {}".format(repr(number)))
    return number


def assert_all_str_or_none(**kwargs):
    """
    Helper function to assert that all arguments are instances of str or None
    """
    bad_args = set()
    for k, v in kwargs.items():
        if v is not None and not isinstance(v, str):
            bad_args.add((k, v))

    if len(bad_args):
        raise TypeError(f"Arguments expected to be str/None, but were: {','.join(f'{repr(k)}:{type(v)}' for k, v in bad_args)}")


def assert_all_str(**kwargs):
    """
    Helper function to assert that all arguments are instances of str
    """
    bad_args = set()
    for k, v in kwargs.items():
        if not isinstance(v, str):
            bad_args.add((k, v))

    if len(bad_args):
        raise TypeError(f"Arguments expected to be str, but were: {','.join(f'{repr(k)}:{type(v)}' for k,v in bad_args)}")


def _get_year():
    return str(dt.datetime.now().year)


class ExplorerResponse:
    def __init__(self, response: Element, original_request: ExplorerRequest):
        self.original_request = original_request
        self.response = response


class XMLProvider(abc.ABC):
    """
    If you want to create some kind of fancy provider utilizing a MySQL database on the backend or something insane,
    subclass this and supply the abstract methods. See UncachedXMLProvider and SingleSubjectXMLProvider.
    """
    @abc.abstractmethod
    def get_course_xml(self, year: str, quarter: str, subject: str, number: str) -> Element:
        """
        Return an XML element from which get_course will function properly
        """
        pass

    @abc.abstractmethod
    def get_subject_xml(self, year: str, quarter: str, subject: str) -> Element:
        """
        Return an XML element from which get_subject_catalog will function properly.
        """
        pass

    @abc.abstractmethod
    def cascades(self):
        """
        Returns whether the response that was just acquired returned a cascading course response, or a course response.
        """
        pass


class UncachedXMLProvider(XMLProvider):
    """
    Great as a naive XML provider. Does no caching whatsoever. For development use only.
    """
    def __init_(self, **kwargs):
        pass

    def get_course_xml(self, year: Optional[str], quarter: Optional[str], subject: Optional[str], number: Optional[str]) -> Element:
        fetch_request = ExplorerRequestFactory.fetch_request_course(year=year, quarter=quarter, subject=subject, number=number)
        return fetch(fetch_request).response

    def get_subject_xml(self, year: str, quarter: str, subject: str) -> Element:
        fetch_request = ExplorerRequestFactory.fetch_request_subject(year=year, quarter=quarter, subject=subject)
        return fetch(fetch_request).response

    def cascades(self):
        return False


class SingleSubjectXMLProvider(XMLProvider):
    """
    Provides a cascaded version of a single course subject. Caches the response as a local variable. Always calls with mode=cascade.
    Because of the mode=cascade, it is possible to extract full course information from the response XML. See get_courses_of_subject
    for an example.

    For development use only.
    """
    def __init__(self, year: str, quarter: str, subject: str):
        self.init_info = ExplorerRequestFactory.fetch_request_subject(year=year, quarter=quarter, subject=subject)
        self.year, self.quarter, self.subject = self.init_info.year, self.init_info.quarter, self.init_info.subject
        self.xml = None

    def load_xml(self):
        fetch_request = ExplorerRequestFactory.fetch_request_subject(year=self.year, quarter=self.quarter, subject=self.subject, cascade=True)
        self.xml = fetch(fetch_request).response

    def get_subject_xml(self, year: str, quarter: str, subject: str) -> Element:
        if self.xml is None:
            self.load_xml()
        self.validate_params(year, quarter, subject)

        return self.xml

    def get_course_xml(self, year: str, quarter: str, subject: str, number: str) -> Element:
        if self.xml is None:
            self.load_xml()

        self.validate_params(year, quarter, subject)
        xml_path = f".//cascadingCourse[@id='{subject.upper()} {number}']"
        return self.xml.find(xml_path)

    def cascades(self):
        return True  # Take a look at load_xml and tell me it *doesn't* cascade.

    def validate_params(self, year: str, quarter: str, subject: str):
        """
        Because this is a highly specialized provider (single-subject), the parameters fed by a user will be ignored.
        Rather than throwing an error when the parameters mismatch, we raise a warning because otherwise, this would be
        a very brittle class.
        """
        init_params = self.init_info.as_tuple()[:2]
        provided_params = ExplorerRequestFactory.fetch_request_subject(year, quarter, subject).as_tuple()[:2]
        if init_params != provided_params:
            warnings.warn(f"{repr(self)} will ignore the implied params {provided_params}", category=UserWarning)

    def __repr__(self):
        init_params = self.init_info.as_tuple()[:2]
        return "<SingleSubjectXMLProvider(year={}, quarter={}, subject={})>".format(*init_params)


default_xml_provider = UncachedXMLProvider()


def fetch(fetch_request: ExplorerRequest):
    """
    Queries official xml api with given parameters.

    :param fetch_request: Fetch request created with ExplorerRequestFactory.
    :bool: Flag to indicate whether to cascade the request or not.
    :return: appropriate ``lxml.etree.Element`` object
    """
    # print(f"Fetch performed: {fetch_request}")
    if not isinstance(fetch_request, ExplorerRequest):
        raise TypeError("Expected fetch_request of type {}, got {}".format(repr(ExplorerRequest), repr(type(fetch_request))))

    xml_raw = requests.get(fetch_request.to_request()).content
    try:
        return ExplorerResponse(etree.fromstring(xml_raw), fetch_request)
    except etree.XMLSyntaxError as ex:
        raise RuntimeError("Could not parse XML response of {}".format(repr(ExplorerRequest))) from ex


def get_subject_catalog(subject: Optional[str], year: Optional[str] = None,
                        quarter: Optional[str] = None, xml_provider = default_xml_provider) -> Iterator[str]:
    """
    Gets all the courses for a given subject.

    :param subject: subject abbreviation
    :param year: year to get catalog from (defaults to current year)
    :param quarter: one of ``{'fall', 'spring', 'winter', 'summer'}``
                    (any case, defaults to spring)
    :return: iterator of course names
    """
    fetch_tmp = ExplorerRequestFactory.fetch_request_subject(subject=subject, year=year, quarter=quarter)
    catalog = xml_provider.get_subject_xml(year=fetch_tmp.year, quarter=fetch_tmp.quarter, subject=fetch_tmp.subject)
    if xml_provider.cascades():
        return (
            "{0}".format(course.attrib["id"]) # id is like "CS 225" here. Don't need to add subject.
            for course in catalog.find("cascadingCourses")
        )
    else:
        return (
            "{0} {1}".format(subject, course.attrib["id"]) # id is like "225" here.
            for course in catalog.find("courses")
        )


def subject_iterator(year: Optional[str] = None, quarter: Optional[str] = None) -> Iterator[str]:
    """
    Gets an iterator of subject names.

    :param year: year to get catalog (defaults to current year)
    :param quarter: one of ``{'fall', 'spring', 'winter', 'summer'}``
                    (any case, defaults to spring)
    :return: generator yielding abbreviated subject names
    """
    fetch_request = ExplorerRequestFactory.fetch_request_quarter(year=year, quarter=quarter)
    catalog = fetch(fetch_request).response
    return (e.attrib["id"] for e in catalog.find("subjects"))


def course_iterator(subjects: Optional[Iterator[str]] = None, max_workers: int = 32) -> Iterator[str]:
    """
    Gets an iterator of courses, high volume of https requests is streamlined through concurrency.

    :param subjects: iterator of subject names, intended to be output of ``get_subjects()``
                     (defaults to all subjects)
    :param max_workers: max number of worker threads to use
    :return: generator yielding course names: ``('AAS 100', 'AAS 105', 'AAS 120'..)``.
    """

    if subjects is None:
        subjects = subject_iterator()
    with ThreadPoolExecutor(max_workers=max_workers) as executor:
        for subject in executor.map(get_subject_catalog, subjects):
            yield from subject

