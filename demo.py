import uiuc_api

# Some sample highest-level API requests.
# Please read the docstrings or type help(any_given_function) for detailed documentation and usages.

print("All courses in philosophy with their credit hours:")
for course in uiuc_api.get_courses_of_subject(year="2021", subject="PHIL"):
    print(course.label, ":", course.hours)

print("Details about CS 225 in Fall 2020:")
print(uiuc_api.get_course("CS 225", year="2020", quarter="fall").serialize())

print("All courses in ECE in Spring of current year:")
for course in uiuc_api.get_subject_catalog(subject="ECE"):
    print(course)

print("Courses in CSE in Spring 2022, redirected if they point to another course")
for course in uiuc_api.get_courses_of_subject(subject="CSE", quarter="spring", year="2022", redirect=True):
    print(course.subject, course.number)

# TODO: Demo the lower-level APIs provided by ExplorerRequestFactory + fetch
